# Vagrant Docker

## Herramientas a instalar
- Virtual Box 6: https://www.virtualbox.org/
- Git: https://git-scm.com/
- Vagrant: https://www.vagrantup.com/downloads.html

## Instalar entorno de prácticas
Después de haber instalado todas las herramientas anteriores, por favor debe seguir los siguientes pasos:
- Crear un directorio llamado **docker**.
- Abrir una consola de comandos y entrar al directorio **docker**.
- Dentro del directorio docker ejecutar el comando: `git clone https://gitlab.com/ceal1818/vagrant-docker.git`
- Cuando termine el comando anterior, ejecutar el comando: `vagrant up`
- Este comando tarda en terminar, pero su resultado deben ser la construcción de 4 maquinas virtuales que terminan arrancadas.
- Para concluir y revisar que todo ha ido bien, ejecuta el comando `vagrant ssh manager`. Este comando te dará acceso a una de las maquinas.
- Dentro de esta maquina ejecuta el comando `docker images`. Si te devuelve una lista donde aparece la palabra **centos**, todo esta OK.