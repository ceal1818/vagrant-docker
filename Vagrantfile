$install_docker_script = <<SCRIPT
echo Installing Docker...
curl -sSL https://get.docker.com/ | sh
usermod -aG docker vagrant
usermod -aG sudo vagrant
SCRIPT

$pull_base_images = <<SCRIPT
echo Download some images...
docker pull httpd
docker pull hello-world
docker pull centos
docker pull alpine
docker pull node:8.16
docker pull nginx
docker pull mysql:5.6
docker pull drhelius/hello-world-node-microservice
SCRIPT

$manager_script = <<SCRIPT
echo Swarm Init...
docker swarm init --listen-addr 10.100.199.200:2377 --advertise-addr 10.100.199.200:2377
docker swarm join-token --quiet worker > /vagrant/worker_token
docker run -ti -d -p 5000:5000 -e HOST=localhost -e PORT=5000 -v /var/run/docker.sock:/var/run/docker.sock dockersamples/visualizer
SCRIPT

$worker_script = <<SCRIPT
echo Swarm Join...
docker swarm join --token $(cat /vagrant/worker_token) 10.100.199.200:2377
SCRIPT

$network_config_script = <<SCRIPT
echo '
127.0.0.1       localhost
10.100.199.200  manager
10.100.199.201  worker01
10.100.199.202  worker02
10.100.199.203  worker03
'> /etc/hosts
SCRIPT

$gluster_node_setup_script = <<SCRIPT
apt-get update && apt-get install software-properties-common -y
add-apt-repository ppa:gluster/glusterfs-6 && apt-get update
SCRIPT

$apt_install_script = <<SCRIPT
apt-get update && apt-get install software-properties-common openssh-server -y
SCRIPT

$ansible_install_script = <<SCRIPT
apt-add-repository ppa:ansible/ansible 
apt-get update && apt-get install ansible -y
SCRIPT

$ssh_manager_config_script = <<SCRIPT
echo "vagrant" | su - vagrant -c "ssh-keygen -t rsa -N '' -f /home/vagrant/.ssh/id_rsa"
SCRIPT


Vagrant.configure('2') do |config|

  vm_box = 'ubuntu/trusty64'

  (1..3).each do |i|
    config.vm.define "worker0#{i}" do |worker|
      worker_extra_hdd = "./temp/worker0#{i}-ext-hdd.vdi"
      worker.vm.box = vm_box
      worker.vm.box_check_update = true
      worker.vm.network :private_network, ip: "10.100.199.20#{i}"
      worker.vm.hostname = "worker0#{i}"
      worker.vm.synced_folder ".", "/vagrant"
      worker.vm.provision "shell", inline: $network_config_script, privileged: true
      worker.vm.provision "shell", inline: $apt_install_script, privileged: true
#      worker.vm.provision "shell", inline: $install_docker_script, privileged: true
#      worker.vm.provision "shell", inline: $pull_base_images, privileged: true
#      worker.vm.provision "shell", inline: $worker_script, privileged: true
      worker.vm.provider "virtualbox" do |vb|
        unless File.exists?(worker_extra_hdd)
          vb.customize ["createmedium", "disk", "--filename", worker_extra_hdd, "--size", 10 * 1024, "--variant", "Fixed"]
        end
        vb.customize ['storageattach', :id, '--storagectl', 'SATAController', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', worker_extra_hdd]       
        vb.name = "worker0#{i}"
        vb.memory = "1024"
      end
    end
  end

  config.vm.define :manager, primary: true  do |manager|
    manager.vm.box = vm_box
    manager.vm.box_check_update = true
    manager.vm.network :private_network, ip: "10.100.199.200"
    manager.vm.network :forwarded_port, guest: 8080, host: 8080
    manager.vm.network :forwarded_port, guest: 5000, host: 5000
    manager.vm.hostname = "manager"
    manager.vm.synced_folder ".", "/vagrant"
    manager.vm.provision "shell", inline: $network_config_script, privileged: true
    manager.vm.provision "shell", inline: $apt_install_script, privileged: true
#    manager.vm.provision "shell", inline: $ssh_manager_config_script, privileged: true
#    manager.vm.provision "shell", inline: $ansible_install_script, privileged: true
#    manager.vm.provision "shell", inline: $install_docker_script, privileged: true
#    manager.vm.provision "shell", inline: $pull_base_images, privileged: true
#    manager.vm.provision "shell", inline: $manager_script, privileged: true
    manager.vm.provider "virtualbox" do |vb|
      vb.name = "manager"
      vb.memory = "1024"
    end
  end

end
